" mousemode
set mouse=a

" navigation
map h <Nop>
noremap ; <Right>
noremap l <Up>
noremap k <Down>
noremap j <Left>

" use the system clipboard as the default register
set clipboard=unnamedplus

" butterfingers
cnoreabbrev Noh noh
cnoreabbrev Q q
cnoreabbrev Q! q!
cnoreabbrev Qa qa
cnoreabbrev Qa! qa!
cnoreabbrev W w
cnoreabbrev Wq wq
cnoreabbrev Wa wa
cnoreabbrev Bn bn
cnoreabbrev Bp bp

" leaders
let mapleader=' '
nnoremap <leader>d :bp<CR>
nnoremap <leader>f :bn<CR>

syntax on
filetype on
